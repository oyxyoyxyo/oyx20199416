#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;
#include <string.h> 
#include <time.h> 
struct infor{
	char name[20];
	char phone[12];
	int size;
}infor[50];
int per=0,re=50;
void sort();
void add();
void deleted();
void modification();
void search();

int menu()
{
	printf("==========通讯录==========\n");
	printf("\n");
	printf("==========界面===========\n");
	printf("人数：%d人   |剩余空间：%d人\n",per,re);
	for(int i=0;i <per;i++)
	{
		printf("编号：%d号|姓名：%s|电话：%s\n",infor[i].size,infor[i].name,infor[i].phone);
	}
}

void sort()
{
	int x;	
	printf("请选择排序的方式\n");
	printf("1) 编号排序  2）姓名排序\n");
	scanf("%d",&x);
	switch(x)
	{
		case 1:
		    for(int i=0;i<per-1;i++)
        	{
			    for(int j=0;j<per-1;j++)
	         	{
		        	if(infor[j].size>infor[j+1].size)
		        	{
		 	        	swap(infor[j],infor[j+1]);
		        	}
	         	}
        	}  
        	break;
		case 2:
			for(int i=0;i<per-1;i++)
	        {
	        	for(int j=0;j<per-1-i;j++)
		        {
			        if(strcmp(infor[i].name,infor[j+1].name)>0)
		        	{
			        	swap(infor[i],infor[j+1]);
		        	}
	        	}
         	}
         	break;
        default:
        	printf("错误操作请重新输入\n");
        	system("cls");
    }
} 

void add()
{
	if(re<0)
	{
		printf("通讯录已满\n");
	}
	else
	{    
	    printf("请输入添加编号：");
     	scanf("%d",&infor[per].size);
	    if (infor[per].size >=50||infor[per].size<1)
	    {
	    	printf("处理编号超过阈值\n");
			system("pause");
			getchar();
			return; 
		}
		else
		{
			for(int i=0;i<per;i++)
        	{
	            if(infor[i].size==infor[per].size)
	            {
		            printf("此处已有数据\n");
		            system("pause");
		            return;
	            }
	        }
	    }
	        printf("请输入联系人姓名：");
            scanf("%s",infor[per].name);
   	        printf("请输入联系人电话：");
	        scanf("%s",infor[per].phone); 
	        per++;
	        re--;
			system("pause");
	}
}

void deleted()
{
	int x;
	printf("请输入删除位置：\n");
	scanf("%d",&x);
	if(x>=50||x<0)
	{
		printf("处理编号超过阈值\n");
	}
	else
	{
		for(int i=0;i<per;i++)
        {
	    	if(infor[i].size==x)
			{ 
			    infor[i].size=0;
	    		per--;
	    		re++;
	    		printf("已成功删除数据\n"); 
	    		system("pause");
	    	}
    	}
	} 
	printf("此处无数据\n");
	system("pause");
}

void modification()
{
	int size0;
	char rename[20],rephone[12];
	printf("请输入修改位置: ");
	scanf("%d",&size0); 
	if(size0>=50||size0<0)
	{
		printf("处理编号超过阈值\n");
		system("pause");
	}
	else
	{
	    for(int i=0;i<per;i++)
	    {
	    	if(infor[i].size==size0)
	    	{
		    	printf("已擦除原有信息，请重新键入\n");
			    printf("请输入联系人姓名：");
			    scanf("%s",rename); 
			    printf("请输入联系人电话：");
		    	scanf("%s",rephone); 
		    	strcpy(infor[i].name ,rename);
		    	strcpy(infor[i].phone ,rephone);
			    system("pause");
			    return;
			}
		}
		printf("此处无数据\n");
     	system("pause");
	}	
}

void search()
{
	char x[20];
	printf("请输入你要查找联系人的姓名或电话号码：");
	scanf("%s",x);
	for(int i=0;i<per;i++)
	{
		if(strcmp(infor[i].name,x)==0 || strcmp(infor[i].phone,x)==0)
		{
			printf("编号：%d|姓名：%s|电话：%s\n",infor[i].size,infor[i].name,infor[i].phone); 
			system("pause");
			return;
		}
	}
	printf("查无此人\n"); 
	system("pause");
}

int main()
{
	int a;
	while(true)
	{
		system("cls");
		menu();
		printf("操作列表：\n1)排序  2）添加  3）删除\n4）修改  5） 查找  6）退出程序\n");
	    printf("请输入操作：\n"); 
		scanf("%d",&a);
		switch(a)
		{
			case 1:sort();break;
			case 2:add();break;
			case 3:deleted();break;
			case 4:modification();break;
			case 5:search();break;
			case 6:printf("退出程序\n");system("pause");
			       return 1;
			default:printf("处理编号超过阈值\n");break;	        
		}
	}   
}

