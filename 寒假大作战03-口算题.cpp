#include <stdio.h>
#include <stdlib.h>
#include <time.h> 
void one();
void two();
void three();
int main (void)
{
	int a=-1;
	printf("==========口算生成器==========\n");
	printf("欢迎使用口算生成器 :\n");
	printf("\n");
	
	printf("帮助信息\n"); 
	printf("您需要输入命令代号来进行操作，且\n");
	printf("一年级题目为不超过十位的加减法;\n");
	printf("二年级题目为不超过百位的乘除法;\n");
	printf("三年级题目为不超过百位的加减乘除混合题目.\n");
	printf("\n");
	
	while(a != 0)
	{
		printf("操作列表：\n1)一年级  2）二年级  3）三年级\n4）帮助  5）退出程序\n请输入操作>");
		scanf("%d",&a);
		printf("\n");
		printf("\n");
		
		switch(a)
		{
			case 1:printf("现在是一年级题目：\n请输入操作个数>");
			       one();
			       printf("\n");break;
			case 2:printf("现在是二年级题目：\n请输入操作个数>");
			       two();
			       printf("\n");break;
			case 3:printf("现在是三年级题目：\n执请输入操作个数>");
			       three();
			       printf("\n");break;
			case 4:printf("帮助信息\n");
                   printf("您需要输入命令代号来进行操作，且\n");
	               printf("一年级题目为不超过十位的加减法;\n");
	               printf("二年级题目为不超过百位的乘除法;\n");
                   printf("三年级题目为不超过百位的加减乘除混合题目.\n");
			       printf("\n");break;
			case 5:a=0;break;
			default:printf("Error!!!\n错误指令，请重新输入\n");
			        printf("\n");break;	        
		}
	}
	 
    printf("程序结束，欢迎下次使用\n任意键结束......");        
	return 0;   
}  
void one()
{
	int n;
	time_t t;
	char f[2] = { '+','-' };
	scanf("%d", &n);
	srand((unsigned) time(&t));
	for (int i = 0; i < n; i++)
	{
		printf("%d %c %d = __\n", rand()%10,f[rand()%2], rand()%10);
	}
}
void two()
{
	int n;
	time_t t;
	char f[] = { '*','/' };
	scanf("%d", &n);
	srand((unsigned) time(&t));
	for (int i = 0; i < n; i++)
	{
		printf("%2d %c %2d = __\n", rand() %100,f[rand()%2], rand()%100);
	}
}
void three()
{
	int n;
	time_t t;
	char f[4] = { '+','-','*','/'};
	scanf("%d",&n);
	srand((unsigned) time(&t));
	for (int i = 0; i < n; i++)
	{
		printf("%2d %c %2d %c %2d = __\n",rand() %100,f[rand()%3],rand() %100,f[rand()%3],rand() %100);
	} 
}

